#!/usr/bin/perl

use strict;
use warnings;

use Time::Piece ();
use Time::Seconds;
use Getopt::Long;
use Scalar::Util qw(looks_like_number);

# distance in minutes between two timestamps
my $gap=3;

# length of x axis in days
my $days=7;

my $timingBoundaryOk=75;
my $timingBoundaryWarn=250;
my $timingBoundaryError=500;

my $renderTiming=0;

GetOptions ("days=i" => \$days,
			"gap=i" => \$gap,
			"timing" => \$renderTiming);

sub usage {
	print <<EOF

	availability.pl [--days n] [--gap min]

Reads a CSV from STDIN and turns it into SVG. The CSV consists of
the columns ISO-Date, HTTP Status and response time. It looks for
example like this:

2021-02-09T10:00:00	200	50
2021-02-09T10:03:00	200	35
2021-02-09T10:06:00	200	50
2021-02-09T10:09:00	200	50
2021-02-09T10:12:00	200	35
2021-02-09T10:15:00	200	50

The SVG is a 2 dimensional graph. Each marker on the x-axis represents
a single day. Each marker on the y-axis is a 15min part of the day's
hours. Every line with a status code of 200 creates a green blip. All
404 turn into yellow blips. Finally 500er status yields a red blip.

Options:

--days n  - number of days starting from first line, extra days are cut off
            empty days yield empty columns.

--gap min - Gap in minutes between status samples.

--timing  - Use timing column instead of HTTP status 

cat availability.csv | availability.pl --days 30 --gap 5 >availability.svg

EOF
}

# bar width per day
my $dayWidth=80;

# an entire day
my $dayHeight=24*60;

# in minutes
my $tickSpacing=15;
my $tickWidth=5;

# space between two bars
my $dayGap=5;

my $fontHeight=20;

# Indent of Y-Axis to the right, make space
# for ticks and values
my $yAxisX=50+$fontHeight;

my $totalHeight=$dayHeight+3*$fontHeight;
my $totalWidth=$yAxisX+(($days+1)*$dayWidth)+($days*$dayGap);

my $axisStroke=2;

print <<EOF
<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" height="1024px" viewBox="0 0 $totalWidth $totalHeight">
<style type="text/css">
    <![CDATA[
	  svg {
	  	font-family:Arial,sans-serif;
		font-size:12pt;
	  }

	  .title-font {
	  	font-size:150%;
	  }

	  .legend-font {
	  	font-size:120%;
	  }

	  .timing-empty {
	  	fill: #a0a0a0;
	  }

	  .http500, .timing-error {
        fill: #df4040;
      }
   	  .http200, .timing-ok {
        fill: #40df40;
      }
   	  .http404, .timing-warn {
        fill: #dfdf40;
      }

	  .axis {
	  	stroke: #000000;
		stroke-width: $axisStroke;
	  }

	  .tick {
	  	stroke: #000000;
		stroke-width: 1;
	  }

	  .tickHour {
	  	stroke: #000000;
		stroke-width: $axisStroke;
	  }
 ]]>
  </style>
EOF
;

print '<text class="legend-font" x="20" y="',$dayHeight/2+100,'" transform="rotate(-90,20,',$dayHeight/2+100,')">Uhrzeit</text>', "\n";
print '<line x1="',$yAxisX,'" y1="0" x2="',$yAxisX,'" y2="', $dayHeight+$axisStroke+1, '" class="axis"/>',"\n";

my $tick=0;
my $class='tick';
my $tickEndX=$yAxisX-$tickWidth;
my $tickHourEndX=$tickEndX-$tickWidth;
my $tickX=0;

while ($tick < $dayHeight) {
	if ($tick % 60 == 0) {
		$class='tickHour';
		print '<text x="',$yAxisX-30,'" y="',$tick,'">',24-($tick / 60),'</text>',"\n";
		$tickX=$tickHourEndX;
	} else {
		$class='tick';
		$tickX=$tickEndX;
	}
	print '<line class="',$class,'" x1="',$yAxisX,'" y1="', $tick, '" x2="',$tickX,'" y2="',$tick,'" />',"\n";
	$tick+=$tickSpacing;
}

print '<line class="',$class,'" x1="',$yAxisX,'" y1="', $dayHeight+$axisStroke+1, '" x2="',$totalWidth,'" y2="',$dayHeight+$axisStroke+1,'" />',"\n";

my $line;
my $currDate='';
my $lastDate;
my $x=$yAxisX+$dayGap;
my $y=0;
my $startDate;
my %statusCount;
my $totalLines=0;
my $totalDays=0;

foreach $line (<STDIN>) {
	chomp $line;
	my ($date, $hour, $minute, $code, $timing) = $line =~ /(\d+-\d+-\d+)T(\d+):(\d+):\d+[^ 	]*\s+(\d+)\s+(.+)/;
	if (!$currDate) {
		$currDate=$date;
	} elsif ( $currDate ne $date ) {
		$x+=$dayWidth+$dayGap;
		$currDate=$date;
		$totalDays++;
	}
	if ( ! $startDate ) {
		$startDate=$date;
	}
	
	last if ($totalDays >= $days);

	$y=$dayHeight - ($hour*60+$minute);
	my $cssClass;
	if ($renderTiming == 0) {
		$cssClass="http$code";
	} else {
		if (! $timing || ! looks_like_number($timing) ) {
			$cssClass='timing-empty';
		} elsif ($timing < $timingBoundaryWarn) {
			$cssClass='timing-ok';
		} elsif ($timing >= $timingBoundaryWarn && $timing < $timingBoundaryError) {
			$cssClass='timing-warn';
		} elsif ($timing >= $timingBoundaryError) {
			$cssClass='timing-error';
		}
	}
	print "<rect x=\"$x\" y=\"$y\" width=\"$dayWidth\" height=\"$gap\" class=\"$cssClass\"/>\n";
	$statusCount{$code}++;
	$totalLines++;
}

my $date = Time::Piece->strptime($startDate,'%Y-%m-%d');

my $dayGapX=$yAxisX+$dayWidth/2-15;
for (my $dayOfs=0;$dayOfs<$days;$dayOfs++) {
	print '<text x="',$dayGapX+($dayOfs * ($dayWidth+$dayGap)),'" y="',$dayHeight+$fontHeight,'">',$date->strftime('%d.%m'),'</text>',"\n";
	$date += ONE_DAY;
}
print '<text x="',$yAxisX,'" y="',$totalHeight-$fontHeight,'">', Time::Piece->strptime($startDate,'%Y-%m-%d')->strftime('%d.%m.%Y'), ' - ', $date->strftime('%d.%m.%Y'), '</text>', "\n";
my $onlinePercent=100.0/$totalLines*$statusCount{200};
my $stats="Online $onlinePercent%";
print '<text x="',$yAxisX,'" y="',$totalHeight,'">',$stats,'</text>',"\n";
print '</svg>',"\n";


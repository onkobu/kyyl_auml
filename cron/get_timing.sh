#!/bin/bash

timestamp=$(date -Iseconds)
begin=$(date +%s.%N)
httpCode=$(curl -so /dev/null -w '%{http_code}' $1)
end=$(date +%s.%N)
millies=$(echo "scale=6; ($end - $begin) * 1000" | bc)
millies=${millies//./,}
echo -ne "$timestamp\t$httpCode\t"
printf '%.0f' $millies
echo

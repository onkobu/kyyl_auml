# kyyl_auml

Your busybody when it comes to crawler based availability logs – kyylä.

1. Periodically lookup a web resource and write timestamp, HTTP status code and total time to a log
1. `cat your_http.log | availability.pl > availability.svg`
1. Enjoy a scalable vector graphic of per-day availability broken down into minutes

You'll need:

* Recent Perl installation
* Minute precise timer job control, e.g. CRON
* HTTP-request triggered by timer job
* a timer job triggering the request every few minutes (3-10)
* a file collecting the statistics per request

This repository contains a few CSV samples under testdata. There is also
a sample Bash script invoking cURL. It does neither use Bash's builtin
`time` command nor the also only second-precise timing of cURL itself. The
latter even broke from time to time with microsecond resolution!

```
./availability.pl <./testdata/availability.csv >availability.svg
```
